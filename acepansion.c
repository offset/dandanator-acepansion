/****** dandanator.acepansion/background ************************************
*
* DESCRIPTION
*  This plugin provides Dandanator emulation to ACE CPC Emulator.
*
*  Partially based on the work of Cesar 'CNGSoft' Nicolas-Gonzalez.
*
* HISTORY
*
*****************************************************************************
*
*/

#include <clib/alib_protos.h>
#include <clib/debug_protos.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>

#include <dos/dos.h>
#include <utility/utility.h>

#include <string.h>

#define USE_INLINE_API
#include <acepansion/lib_header.h>
#include <libraries/acepansion_plugin.h>

#include "acepansion.h"
#include "interface.h"
#define CATCOMP_NUMBERS
#include "generated/locale_strings.h" /* catalog string ids */



struct Library *DOSBase;
struct Library *UtilityBase;



struct PluginData
{
    // Plugin generic part (do NEVER put anything before this!)
    struct ACEpansionPlugin common;

    // Dandanator specifics
    struct SignalSemaphore *sema;

    UBYTE   rom[0x80000];

    UBYTE   regA0;
    UBYTE   regA1;

    UBYTE   slotZ0;
    UBYTE   slotZ1;

    UBYTE   gateArrayRMR;

    LONG    pfxCount;

    UBYTE   commandMode;
    UBYTE   commandId;
    UBYTE   commandValue;

    UBYTE   writePhase;

    STRPTR  romFile;
    BOOL    romModified;
    ULONG   startMode;
    BOOL    resetTrigger;
};



//#define CMD2STR(c) ((c) == CMD_ID_ZONE0 ? "CMD_ID_ZONE0" : (c) == CMD_ID_ZONE1 ? "CMD_ID_ZONE1" : (c) == CMD_ID_CFG ? "CMD_ID_CFG" : "CMD_ID_???")

#define REGA0_Z0_FOLLOW_ROMEN_SLOT    (24)
#define REGA0_OUTPUT_BIT_TO_USB       ( 4)
#define REGA0_EEPROM_WRITE_ENABLE     ( 2)
#define REGA0_SERIAL_PORT_ENABLE      ( 1)

#define REGA1_WAIT_FOR_RET            (64)
#define REGA1_DISABLE_FURTHER_COMMAND (32)
#define REGA1_ENABLE_FOLLOW_ROMEN     (16)
#define REGA1_Z1_A15                  ( 8)
#define REGA1_Z0_A15                  ( 4)
#define REGA1_Z1_DISABLED             ( 2)
#define REGA1_Z0_DISABLED             ( 1)

#define CMD_PREFIX   0xfd // x3 prefix for commands

#define CMD_ID_ZONE0    0x70 // LD (IY+x),B
#define CMD_ID_ZONE1    0x71 // LD (IY+x),C
#define CMD_ID_CFG      0x77 // LD (IY+x),A

#define CMD_ID_SERIALIN 0x7e // LD A,(HL)
#define CMD_ID_RET      0xc9 // RET

#define CMD_MODE_NONE          0
#define CMD_MODE_VALUE_PENDING 1
#define CMD_MODE_DELAYED       2

#define STARTMODE_GAME  0
#define STARTMODE_TEST  1
#define STARTMODE_BASIC 2

#define DEFAULT_ROMPATH "Dandanator/"

#define TOOLTYPE_STARTMODE "STARTMODE"
#define TOOLTYPE_ROMFILE   "ROMFILE"

#define TOOLTYPE_STARTMODE_GAME  "Game"
#define TOOLTYPE_STARTMODE_TEST  "Test"
#define TOOLTYPE_STARTMODE_BASIC "Basic"



static VOID init(struct PluginData *myPlugin);
static VOID doCommand(struct PluginData *myPlugin);
static VOID doWritePhase(struct PluginData *myPlugin, LONG flashAddress, UBYTE flashData);
static BOOL loadROM(struct PluginData *myPlugin, CONST_STRPTR romFile);
static BOOL saveROM(struct PluginData *myPlugin);
static BOOL checkROMModified(struct PluginData *myPlugin);




/*
** Private function which is called to initialize commons
** just after the library was loaded into memory and prior
** to any other API call.
**
** This is the good place to open our required libraries.
*/
BOOL InitResources(VOID)
{
    DOSBase = OpenLibrary("dos.library", 0L);
    UtilityBase = OpenLibrary("utility.library", 0L);

    return DOSBase != NULL && UtilityBase != NULL
        && GUI_InitResources();
}



/*
** Private function which is called to free commons
** just before the library is expurged from memory
**
** This is the good place to close our required libraries.
*/
VOID FreeResources(VOID)
{
    GUI_FreeResources();

    CloseLibrary(DOSBase);
    CloseLibrary(UtilityBase);
}



/****** dandanator.acepansion/CreatePlugin **********************************
*
* NAME
*   CreatePlugin -- (V1) -- GUI
*
* SYNOPSIS
*   struct ACEpansionPlugin * CreatePlugin(CONST_STRPTR *toolTypes, struct SignalSemaphore *sema)
*
* FUNCTION
*   Create the plugin data structure and configure how it will be handled by
*   ACE. This function is usually called at ACE startup.
*
* INPUTS
*   *toolType
*     A pointer to a NULL terminated array of CONST_STRPTR containing the
*     tooltypes provided with the plugin library.
*   *sema
*     Pointer to a semaphore to use to protect access to custom data from
*     ACEpansionPlugin when accessed from subtasks or within your PrefsWindow.
*
* RESULT
*     A pointer to an allocated and initialized plugin data structure.
*
* SEE ALSO
*   DeletePlugin
*   ActivatePlugin
*   GetPrefsPlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
struct ACEpansionPlugin * CreatePlugin(CONST_STRPTR *toolTypes, struct SignalSemaphore *sema)
{
    // We are allocating an extended plugin structure to add our custom stuff
    struct PluginData *myPlugin = (struct PluginData*)AllocVec(sizeof(struct PluginData), MEMF_PUBLIC|MEMF_CLEAR);

    if(myPlugin)
    {
        // Get tooltypes to configure ourself
        if(toolTypes) while(*toolTypes)
        {
            CONST_STRPTR ptr = *toolTypes;
            CONST_STRPTR name = ptr;
            ULONG len = 0;

            if(*ptr != '(')
            {
                while(*ptr != '=' && *ptr != '\0')
                {
                    ptr++;
                    len++;
                }

                if(*ptr == '=')
                {
                    if(Strnicmp(name, TOOLTYPE_ROMFILE, len) == 0)
                    {
                        myPlugin->romFile = StrDup(ptr+1);
                    }
                    else if(Strnicmp(name, TOOLTYPE_STARTMODE, len) == 0)
                    {
                        CONST_STRPTR value = ptr+1;

                        if(Stricmp(value, TOOLTYPE_STARTMODE_GAME) == 0)
                        {
                            myPlugin->startMode = STARTMODE_GAME;
                        }
                        else if(Stricmp(value, TOOLTYPE_STARTMODE_TEST) == 0)
                        {
                            myPlugin->startMode = STARTMODE_TEST;
                        }
                        else if(Stricmp(value, TOOLTYPE_STARTMODE_BASIC) == 0)
                        {
                            myPlugin->startMode = STARTMODE_BASIC;
                        }
                    }
                }
            }
            toolTypes++;
        }
        // Initialize plugin specifics
        myPlugin->sema = sema;
        init(myPlugin);

        // Initialize plugin generic part
        myPlugin->common.ap_APIVersion     = API_VERSION;
        myPlugin->common.ap_Flags          = ACE_FLAGSF_ACTIVE_RESET
                                           | ACE_FLAGSF_ACTIVE_EMULATE
                                           | ACE_FLAGSF_ACTIVE_WRITEIO
                                           | ACE_FLAGSF_ACTIVE_READMEM
                                           | ACE_FLAGSF_ACTIVE_WRITEMEM
                                           | ACE_FLAGSF_SAVE_PREFS;
        myPlugin->common.ap_Title          = GetString(MSG_TITLE);
        myPlugin->common.ap_HelpFileName   = LIBNAME".guide";
        myPlugin->common.ap_ToggleMenuName = GetString(MSG_MENU_TOGGLE);
        myPlugin->common.ap_PrefsMenuName  = GetString(MSG_MENU_PREFS);
        myPlugin->common.ap_PrefsWindow    = GUI_Create((struct ACEpansionPlugin *)myPlugin);

        if(myPlugin->common.ap_PrefsWindow)
        {
            GUI_SetStartMode(myPlugin->common.ap_PrefsWindow, myPlugin->startMode);
            GUI_SetROMFile(myPlugin->common.ap_PrefsWindow, myPlugin->romFile);
        }
        else
        {
            DeletePlugin((struct ACEpansionPlugin *)myPlugin);
            myPlugin = NULL;
        }
    }

    return (struct ACEpansionPlugin *)myPlugin;
}



/****** dandanator.acepansion/DeletePlugin **********************************
*
* NAME
*   DeletePlugin -- (V1) -- GUI
*
* SYNOPSIS
*   VOID DeletePlugin(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Delete a plugin data structure. This function is usually called when ACE
*   is exiting.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to delete.
*
* RESULT
*
* SEE ALSO
*   CreatePlugin
*   ActivatePlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID DeletePlugin(struct ACEpansionPlugin *plugin)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if(!checkROMModified(myPlugin))
    {
        GUI_NotifyROMSaveError(myPlugin->common.ap_PrefsWindow, myPlugin->romFile);
    }
    GUI_Delete(myPlugin->common.ap_PrefsWindow);
    FreeVec(myPlugin->romFile);
    FreeVec(myPlugin);
}



/****** dandanator.acepansion/ActivatePlugin ********************************
*
* NAME
*   ActivatePlugin -- (V1) -- GUI
*
* SYNOPSIS
*   BOOL ActivatePlugin(struct ACEpansionPlugin *plugin, BOOL status)
*
* FUNCTION
*   This function is called everytime ACE wants to activate or disactivate your plugin.
*   While you are disactivated, no other function (except Delete) will the called by ACE.
*   Please note that a plugin is always disactivated by ACE prior to DeletePlugin().
*
* INPUTS
*   activate
*     New activation status requested by ACE.
*
* RESULT
*   Actual activation status (you may fail at activation for some reason, but you
*   should never fail at disactivation request).
*
* SEE ALSO
*   CreatePlugin
*   DeletePlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
BOOL ActivatePlugin(struct ACEpansionPlugin *plugin, BOOL activate)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if(activate)
    {
        // Load given ROM at activate only to speed-up startup
        if(myPlugin->romFile)
        {
            if(!loadROM(myPlugin, myPlugin->romFile))
            {
                GUI_NotifyROMLoadError(myPlugin->common.ap_PrefsWindow, myPlugin->romFile);
                GUI_SetROMFile(myPlugin->common.ap_PrefsWindow, DEFAULT_ROMPATH);
            }
        }
    }

    return activate;
}



/****** dandanator.acepansion/GetPrefsPlugin ********************************
*
* NAME
*   GetPrefsPlugin -- (V5) -- GUI
*
* SYNOPSIS
*   STRPTR * GetPrefsPlugin(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function that is used by ACE to know about the current preferences of the
*   plugins in order to save them.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   pool
*     Memory pool to use to allocate the tooltypes.
*
* RESULT
*   Pointer to a NULL terminated string array containing the tooltypes or NULL
*   when no tooltypes.
*
* SEE ALSO
*   CreatePlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
STRPTR * GetPrefsPlugin(struct ACEpansionPlugin *plugin, APTR pool)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;
    STRPTR *tooltypes = (STRPTR*)AllocPooled(pool, 3*sizeof(STRPTR));

    if(tooltypes)
    {
        LONG idx = 0;

        switch(myPlugin->startMode)
        {
            case STARTMODE_GAME:
                tooltypes[idx++] = MakeToolType(pool, TOOLTYPE_STARTMODE, TOOLTYPE_STARTMODE_GAME);
                break;
            case STARTMODE_TEST:
                tooltypes[idx++] = MakeToolType(pool, TOOLTYPE_STARTMODE, TOOLTYPE_STARTMODE_TEST);
                break;
            case STARTMODE_BASIC:
                tooltypes[idx++] = MakeToolType(pool, TOOLTYPE_STARTMODE, TOOLTYPE_STARTMODE_BASIC);
                break;
        }

        if(myPlugin->romFile)
        {
            tooltypes[idx++] = MakeToolType(pool, TOOLTYPE_ROMFILE, myPlugin->romFile);
        }

        tooltypes[idx] = NULL;
    }

    return tooltypes;
}



/****** dandanator.acepansion/Reset *****************************************
*
* NAME
*   Reset -- (V1) -- EMU
*
* SYNOPSIS
*   VOID Reset(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function that is called everytime ACE is issuing a bus reset.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*
* RESULT
*
* SEE ALSO
*   Emulate
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Reset(struct ACEpansionPlugin *plugin)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    init(myPlugin);
}



/****** dandanator.acepansion/Emulate ***************************************
*
* NAME
*   Emulate -- (V1) -- EMU
*
* SYNOPSIS
*   VOID Emulate(struct ACEpansionPlugin *plugin, ULONG *signals)
*
* FUNCTION
*   Function that is called at each microsecond of the emulation step inside ACE.
*   Note: Implement this function only if is it really required for your plugin because it
*   is obviously CPU intensive.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   *signals
*     Pointer to the I/O signals from the expansion port you can use or alter
*     depending on what you are emulating.
*     The signals bits can be decoded using the following flags:
*       ACE_SIGNALF_INT -- (V1)
*         Maskable interrupt; corresponding to /INT signal from CPC expansion
*         port.
*       ACE_SIGNALF_NMI -- (V3)
*         Non-maskable interrupt; corresponding to /NMI signal from CPC
*         expansion port.
*       ACE_SIGNALF_LPEN -- (V4)
*         Light pen trigger; corresponding to /LPEN signal from CPC expansion
*         port.
*       ACE_SIGNALF_BUS_RESET -- (V6)
*         Bus reset trigger; correspond to /BUSRESET signal from CPC expansion
*         port.
*       ACE_SIGNALF_NOT_READY -- (V6)
*         Not ready trigger; correspond to READY signal from CPC expansion port.
*
* RESULT
*
* SEE ALSO
*   Reset
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Emulate(struct ACEpansionPlugin *plugin, ULONG *signals)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if(myPlugin->resetTrigger)
    {
        *signals |= ACE_SIGNALF_BUS_RESET;

        myPlugin->resetTrigger = FALSE;
    }
}



/****** dandanator.acepansion/WriteIO ***************************************
*
* NAME
*   WriteIO -- (V1) -- EMU
*
* SYNOPSIS
*   VOID WriteIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE value)
*
* FUNCTION
*   Function that is called everytime Z80 is performing a I/O port write operation.
*   Note: Implement this function only if is it really required for your plugin because it
*   is obviously CPU intensive.
*
* INPUTS
*   port
*     Z80 port on with the I/O write operation was issued.
*   value
*     Value that was issued on the port.
*
* RESULT
*
* SEE ALSO
*   ReadIO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID WriteIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE value)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    // Gate Array snifing
    if((port & 0xc000) == 0x4000)
    {
        if((value & 0xc0) == 0x80)
        {
            myPlugin->gateArrayRMR = value;
        }
    }
}



/****** dandanator.acepansion/ReadIO ****************************************
*
* NAME
*   ReadIO -- (V1) -- EMU
*
* SYNOPSIS
*   VOID ReadIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE *value)
*
* FUNCTION
*   Function that is called everytime Z80 is performing a I/O port read operation.
*   Note: Implement this function only if is it really required for your plugin because it
*   is obviously CPU intensive.
*
* INPUTS
*   port
*     Z80 port on with the I/O read operation was issued.
*   *value
*     Value to be returned.
*
* RESULT
*
* SEE ALSO
*   WriteIO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID ReadIO(UNUSED struct ACEpansionPlugin *plugin, UNUSED USHORT port, UNUSED UBYTE *value)
{
}



/****** dandanator.acepansion/WriteMem **************************************
*
* NAME
*  WriteMem -- (V6) -- EMU
*
* SYNOPSIS
*  BOOL WriteMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE value)
*
* FUNCTION
*  Function that is called everytime Z80 is performing a memory write
*  operation.
*
* INPUTS
*  *plugin
*    Pointer to the plugin data structure to use.
*  address
*    Z80 address on which the memory write operation was issued.
*  value
*    Value that has to be written.
*
* RESULT
*  TRUE if the plugins actually cougth the memory write operation so that
*  internal memory will be left unchanged (/RAMDIS is set to 0).
*
* NOTES
*  Implement this function only if is it really required for your plugin
*  because it is obviously CPU intensive.
*
* SEE ALSO
*  ReadMem()
*
*****************************************************************************
*
*/

ACEPANSION_API
BOOL WriteMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE value)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if(myPlugin->romFile
    && (myPlugin->regA1 & REGA1_DISABLE_FURTHER_COMMAND) == 0)
    {
        if(myPlugin->commandMode == CMD_MODE_VALUE_PENDING)
        {
            myPlugin->commandValue = value;

            // Handle delayed REG_A1 update
            if(myPlugin->commandId == CMD_ID_CFG
            && (myPlugin->commandValue & (0x80|REGA1_WAIT_FOR_RET)) == (0x80|REGA1_WAIT_FOR_RET))
            {
                myPlugin->commandMode = CMD_MODE_DELAYED;
            }
            else
            {
                //kprintf("[WR] -> executed immediate command: %s (&%02lx)\n", CMD2STR(myPlugin->commandId), myPlugin->commandValue);
                doCommand(myPlugin);
            }
        }

        // Handle write to flash
        if((myPlugin->regA0 & REGA0_EEPROM_WRITE_ENABLE) == 0)
        {
            if((myPlugin->regA1 & REGA1_Z0_DISABLED) == 0 && (address & 0x4000) == 0)
            {
                if(((address & 0x8000) != 0 && (myPlugin->regA1 & REGA1_Z0_A15) != 0)
                || ((address & 0x8000) == 0 && (myPlugin->regA1 & REGA1_Z0_A15) == 0))
                {
                    doWritePhase(myPlugin, (myPlugin->slotZ0 << 14) | (address & 0x3fff), value);
                }
            }
            else if((myPlugin->regA1 & REGA1_Z1_DISABLED) == 0 && (address & 0x4000) != 0)
            {
                if(((address & 0x8000) != 0 && (myPlugin->regA1 & REGA1_Z1_A15) != 0)
                || ((address & 0x8000) == 0 && (myPlugin->regA1 & REGA1_Z1_A15) == 0))
                {
                    doWritePhase(myPlugin, (myPlugin->slotZ1 << 14) | (address & 0x3fff), value);
                }
            }
        }
    }

    return FALSE;
}



/****** dandanator.acepansion/ReadMem ***************************************
*
* NAME
*  ReadMem -- (V6) -- EMU
*
* SYNOPSIS
*  VOID ReadMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE *value, BOOL opcodeFetch) -- (V7)
*  BOOL ReadMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE *value, BOOL opcode) ** DEPRECATED in V7 **

* FUNCTION
*  Function that is called everytime Z80 is performing a memory read
*  operation.
*
* INPUTS
*  *plugin
*    Pointer to the plugin data structure to use.
*  address
*    Z80 address on which memory read operation was issued.
*  *value
*    Value to be returned.
*    Starting with V7, it contains by default the value that might be read by
*    the CPC if the plugin won't catch the memory read, so that a plugin can
*    only snif the bus.
*  opcodeFetch
*    Boolean which is TRUE when the memory read operation is related to
*    Z80 opcode fetching (/M1 signal). If FALSE, then this is a regular
*    read operation during opcode execution.
*
* RESULT
*  ** DEPRECATED in V7 **
*  TRUE if the plugins actually cougth the memory read operation so that
*  internal memory won't be read (both /ROMDIS and /RAMDIS are set to 0).
*
* NOTES
*  Implement this function only if is it really required for your plugin
*  because it is obviously CPU intensive.
*
* SEE ALSO
*  WriteMem()
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID ReadMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE *value, BOOL opcodeFetch)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if(myPlugin->romFile)
    {
        if((myPlugin->regA1 & REGA1_DISABLE_FURTHER_COMMAND) == 0)
        {
            if((myPlugin->regA1 & REGA1_Z0_DISABLED) == 0 && (address & 0x4000) == 0)
            {
                if(((address & 0x8000) != 0 && (myPlugin->regA1 & REGA1_Z0_A15) != 0)
                || ((address & 0x8000) == 0 && (myPlugin->regA1 & REGA1_Z0_A15) == 0))
                {
                    *value = myPlugin->rom[(myPlugin->slotZ0 << 14) | (address & 0x3fff)];
                }
            }
            else if((myPlugin->regA1 & REGA1_Z1_DISABLED) == 0 && (address & 0x4000) != 0)
            {
                if(((address & 0x8000) != 0 && (myPlugin->regA1 & REGA1_Z1_A15) != 0)
                || ((address & 0x8000) == 0 && (myPlugin->regA1 & REGA1_Z1_A15) == 0))
                {
                    *value = myPlugin->rom[(myPlugin->slotZ1 << 14) | (address & 0x3fff)];
                }
            }
        }
        // "poor-man" rombox: "CPCSOCCER" overrides the current firmware with its own copy
        else if((myPlugin->regA1 & REGA1_ENABLE_FOLLOW_ROMEN))
        {
            if((address & 0xc000) == 0x0000 && (myPlugin->gateArrayRMR & 4) == 0)
            {
                *value = myPlugin->rom[0x70000 | ((myPlugin->regA0 & REGA0_Z0_FOLLOW_ROMEN_SLOT) << 11) | (address & 0x3fff)];
            }
            else if((address & 0xc000) == 0xc000 && (myPlugin->gateArrayRMR & 8) == 0)
            {
                *value = myPlugin->rom[(myPlugin->slotZ1 << 14) | (address & 0x3fff)];
            }
        }

        // Snifing commands
        if(opcodeFetch)
        {
            if((myPlugin->regA1 & REGA1_DISABLE_FURTHER_COMMAND) == 0)
            {
                switch(*value)
                {
                case CMD_ID_ZONE0:
                case CMD_ID_ZONE1:
                case CMD_ID_CFG:
                    if(myPlugin->pfxCount > 2)
                    {
                        //kprintf("[RD] Command detected: %s\n", CMD2STR(*value));
                        myPlugin->commandId = *value;
                        myPlugin->commandMode = CMD_MODE_VALUE_PENDING;
                    }
                    return;
                case CMD_ID_SERIALIN:
                    if(myPlugin->regA0 & REGA0_SERIAL_PORT_ENABLE)
                    {
                        //kprintf("[RD] Command executed: SERIALIN\n");
                        // Bit 0 should be set to current serial input status
                        *value = 0xfe;
                    }
                    return;
                case CMD_ID_RET:
                    if(myPlugin->commandMode == CMD_MODE_DELAYED)
                    {
                        //kprintf("[RD] Command detected: RET\n");
                        //kprintf("[WR] -> executed delayed command: %s (&%02lx)\n", CMD2STR(myPlugin->commandId), myPlugin->commandValue);
                        doCommand(myPlugin);
                    }
                    return;
                case CMD_PREFIX:
                    myPlugin->pfxCount++;
                    //if(myPlugin->pfxCount > 2) kprintf("[RD] Prefix detected!\n");
                    return;
                }
            }
        }

        myPlugin->pfxCount = 0;
    }
}



/****** dandanator.acepansion/GetAudio **************************************
*
* NAME
*   GetAudio -- (V1) -- EMU
*
* SYNOPSIS
*   VOID GetAudio(struct ACEpansionPlugin *plugin, SHORT *leftSample, SHORT *rightSample)
*
* FUNCTION
*   Function to be used when your plugin needs to mix an audio output with the one
*   from ACE.
*   It is called at each PSG emulation step, which means at 125KHz.
*
* INPUTS
*   *leftSample
*     Pointer to store the left audio sample to be mixed with ACE audio.
*   *rightSample
*     Pointer to store the right audio sample to be mixed with ACE audio.
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID GetAudio(UNUSED struct ACEpansionPlugin *plugin, UNUSED SHORT *leftSample, UNUSED SHORT *rightSample)
{
}



/****** dandanator.acepansion/Printer ***************************************
*
* NAME
*   Printer -- (V1) -- EMU
*
* SYNOPSIS
*   VOID WritePrinter(struct ACEpansionPlugin *plugin, UBYTE data, BOOL strobe, BOOL *busy)
*
* FUNCTION
*   Function that is called everytime the CPC is accessing the printer port.
*
* INPUTS
*   data
*     8 bits written value (note that CPC is limited to 7 bits values, only Amstrad Plus
*     can actually provide the 8th bit).
*   strobe
*     Status of the /STROBE signal from the printer port.
*   *busy
*     Busy signal value that can be altered.
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Printer(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE data, UNUSED BOOL strobe, UNUSED BOOL *busy)
{
}



/****** dandanator.acepansion/Joystick **************************************
*
* NAME
*   Joystick -- (V2) -- EMU
*
* SYNOPSIS
*   VOID Joystick(struct ACEpansionPlugin *plugin, BOOL com1, BOOL com2, UBYTE *ioData)
*
* FUNCTION
*   Function which is called everytime the CPC is accessing the joystick port.
*   Please note that the joystick port in R/W but you cannot know if the CPC
*   is actually reading or writing to it.
*
* INPUTS
*   com1
*     If com1 is active then the CPC is requesting access to joystick 0.
*   com2
*     If com2 is active then the CPC is requesting access to joystick 1.
*   *ioData
*     If the CPC is reading the joystick port (regular case) ioData will be set
*     to 0xff and you are supposed to set to 0 the bits corresponding to the
*     requested joystick (you should use the ACE_IODATAF_JOYSTICK_XXX defines).
*     Please note the additional ACE_IODATAF_JOYSTICK_PAUSE which is not
*     existing on CPC but that you could use to map the play/pause button of
*     a joystick used in ACE on the 'P' key of the keyboard (or the GX4000
*     pause button).
*     If the CPC is writing the joystick port the ioData will contain the
*     written value to the port and modifying it will have not effect (on a
*     real CPC it could simply destroy the PSG chipset!).
*
* RESULT
    To emulate the analog joystick port of the Amstrad Plus and the GX-4000,
    this function should be used together with AnalogInput().
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Joystick(UNUSED struct ACEpansionPlugin *plugin, UNUSED BOOL com1, UNUSED BOOL com2, UNUSED UBYTE *ioData)
{
}



/****** dandanator.acepansion/AnalogInput ***********************************
*
* NAME
*   AnalogInput -- (V7) -- EMU
*
* SYNOPSIS
*   VOID AnalogInput(struct ACEpansionPlugin *plugin, UBYTE channel, UBYTE *value)
*
* FUNCTION
*   Function which is called everytime the CPC is accessing the analog joystick
*   port of the Amstrad Plus or the GX-4000.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   channel
*     Analog channel number (from 0 to 3).
*   *value
*     Pointer where to store the 6-bit wide value of the data on the channel.
*
* RESULT
*
* NOTES
*   To emulate the analog joystick port of the Amstrad Plus and the GX-4000,
*   this function should be used together with Joystick() which is providing
*   fire buttons (they are shared between both analog and digital joystick
*   ports).
*
* SEE ALSO
*   Joystick()
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID AnalogInput(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE channel, UNUSED UBYTE *value)
{
}



/****** dandanator.acepansion/AcknowledgeInterrupt **************************
*
* NAME
*   AcknowledgeInterrupt -- (V1) -- EMU
*
* SYNOPSIS
*   VOID AcknowledgeInterrupt(struct ACEpansionPlugin *plugin, UBYTE *ivr)
*
* FUNCTION
*   Function which is called everytime the Z80 is entering an interrupt.
*
* INPUTS
*   *ivr
*     IVR could be set to the desired value (Interrupt Vector Register).
*
* RESULT
*
* SEE ALSO
*   ReturnInterrupt
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID AcknowledgeInterrupt(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE *ivr)
{
}



/****** dandanator.acepansion/ReturnInterrupt *******************************
*
* NAME
*   ReturnInterrupt -- (V3) -- EMU
*
* SYNOPSIS
*   VOID ReturnInterrupt(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function which is called everytime the Z80 is returning from an interrupt
*   using RETI instruction.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*   AcknowledgeInterrupt
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID ReturnInterrupt(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** dandanator.acepansion/Cursor ****************************************
*
* NAME
*   Cursor -- (V1) -- EMU
*
* SYNOPSIS
*   VOID Cursor(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function which is called everytime the cursor signal from the CRTC is
*   active.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Cursor(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** dandanator.acepansion/HostGamepadList *******************************
*
* NAME
*   HostGamepadList -- (V7) -- GUI

* SYNOPSIS
*   VOID HostGamepadList(struct ACEpansionPlugin *plugin, STRPTR *list)
*
* FUNCTION
*   This function is called everytime a gamepad in plugged or unplugged from
*   the host so that the plugin can know about the available devices. Index
*   from HostGamepadEvent() will refer to this list.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   list
*     NULL terminated array of strings containing the human readable names of
*     the plugged gamepads, the first one being the localized string for "No
*     joystick". Please note that the contents of this list remains valid
*     until the function is invoked again, so that you can use it safely in
*     your code without duplication.
*
* RESULT
*
* NOTES
*
* SEE ALSO
*   HostGamepadList()
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostGamepadList(UNUSED struct ACEpansionPlugin *plugin, UNUSED STRPTR *list)
{
}



/****** dandanator.acepansion/HostGamepadEvent ******************************
*
* NAME
*   HostGamepadEvent -- (V7) -- GUI
*
* SYNOPSIS
*   VOID HostGamepadEvent(struct ACEpansionPlugin *plugin, UBYTE index, USHORT buttons, BYTE ns, BYTE ew, BYTE lx, BYTE ly, BYTE rx, BYTE ry)
*
* FUNCTION
*   This function is called everytime the state of a host's gamepad state is
*   changing. It let you handle both analogic and digital devices.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   index
*     Index of the gamepad in gamepad list.
*   buttons
*     State of the gamepad buttons.
*     Buttons bits can be decoded using the ACE_HOSTGAMEPADF_BUTTON_XXX flags.
*   ns
*     Horizontal position of directional buttons from -128 to +127.
*   ew
*     Vertical position of directional buttons from -128 to +127.
*   lx
*     Horizontal position of left stick from -128 to +127.
*   ly
*     Vertical position of left stick from -128 to +127.
*   rx
*     Horizontal position of right stick from -128 to +127.
*   ry
*     Vertical position of right stick from -128 to +127.
*
* RESULT
*
* NOTES
*
* SEE ALSO
*   HostGamepadList()
*   libraries/acepansion_plugin.h
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostGamepadEvent(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE index, UNUSED USHORT buttons, UNUSED BYTE ns, UNUSED BYTE ew, UNUSED BYTE lx, UNUSED BYTE ly, UNUSED BYTE rx, UNUSED BYTE ry)
{
}



/****** dandanator.acepansion/HostMouseEvent ********************************
*
* NAME
*   HostMouseEvent -- (V7) -- GUI

* SYNOPSIS
*   VOID HostMouseEvent(struct ACEpansionPlugin *plugin, UBYTE buttons, SHORT deltaX, SHORT deltaY)

* FUNCTION
*   Function that is called everytime a host's mouse event occured

* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   buttons
*     State of the mouse buttons.
*     The mouse buttons bits can be decoded using the following flags:
*       ACE_HOSTMOUSEF_BUTTON_MAIN
*         State of main mouse button.
*       ACE_HOSTMOUSEF_BUTTON_SECOND
*         State of second mouse button.
*       ACE_HOSTMOUSEF_BUTTON_MIDDLE
*         State of middle mouse button.
*   deltaX
*     Horizontal mouse move until previous event.
*   deltaY
*     Vertical mouse move until previous event.
*
* RESULT
*
* NOTES
*
* SEE ALSO
      libraries/acepansion_plugin.h
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostMouseEvent(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE buttons, UNUSED SHORT deltaX, UNUSED SHORT deltaY)
{
}



/****** dandanator.acepansion/HostLightDeviceDiodePulse *********************
*
* NAME
*   HostLightDeviceDiodePulse -- (V7) -- EMU
*
* SYNOPSIS
*   VOID HostLightDeviceDiodePulse(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   This function is called at each photo-diode pulse, when the beam from
*   the screen monitor is just in front of the light device photo-diode.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*   HostLightDeviceButton
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostLightDeviceDiodePulse(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** dandanator.acepansion/HostLightDeviceButton *************************
*
* NAME
*   HostLightDeviceButton -- (V7) -- GUI
*
* SYNOPSIS
*   VOID HostLightDeviceButton(struct ACEpansionPlugin *plugin, BOOL pressed)
*
* FUNCTION
*   This function is called everytime the state of the light device button
*   is changing.
*
* INPUTS
*   pressed
*     New state of the light device button.
*
* RESULT
*
* NOTE
*   When a light device plugin is activated, ACE is mapping this button on
*   mouse main button, so that on-screen click will emulate light device
*   button.
*
* SEE ALSO
*   HostLightDeviceDiodePulse
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostLightDeviceButton(UNUSED struct ACEpansionPlugin *plugin, UNUSED BOOL pressed)
{
}



/*
** Dandanator stuff
*/

static VOID init(struct PluginData *myPlugin)
{
    myPlugin->gateArrayRMR = 0x00;

    myPlugin->regA0 = REGA0_OUTPUT_BIT_TO_USB; // | (3 << 3);
    myPlugin->regA1 = REGA1_Z1_DISABLED;

    myPlugin->slotZ0 = 0;
    myPlugin->slotZ1 = 0;

    myPlugin->commandMode = CMD_MODE_NONE;
    myPlugin->writePhase = 0;

    switch(myPlugin->startMode)
    {
    case STARTMODE_GAME:
        break;
    case STARTMODE_TEST:
        myPlugin->slotZ0 = 31;
        break;
    case STARTMODE_BASIC:
        myPlugin->regA1 |= REGA1_Z0_DISABLED;
        break;
    }
}

static VOID doCommand(struct PluginData *myPlugin)
{
    switch(myPlugin->commandId)
    {
        case CMD_ID_ZONE0:
            // Bit 5 is a mirror of Z0 disabled bit of A1
            if(myPlugin->commandValue & 0x20)
            {
                myPlugin->regA1 |= REGA1_Z0_DISABLED;
            }
            else
            {
                myPlugin->regA1 &= ~REGA1_Z0_DISABLED;
            }
            // Bits 0-4 are defining the slot to be used
            myPlugin->slotZ0 = myPlugin->commandValue & 0x1f;
            break;
        case CMD_ID_ZONE1:
            // Bit 5 is a mirror of Z1 disabled bit of A1
            if(myPlugin->commandValue & 0x20)
            {
                myPlugin->regA1 |= REGA1_Z1_DISABLED;
            }
            else
            {
                myPlugin->regA1 &= ~REGA1_Z1_DISABLED;
            }
            // Bits 0-4 are defining the slot to be used
            myPlugin->slotZ1 = myPlugin->commandValue & 0x1f;
            break;
        case CMD_ID_CFG:
            if(myPlugin->commandValue & 0x80)
            {
                myPlugin->regA1 = myPlugin->commandValue;
                // If REGA1_WAIT_FOR_RET is not set then REGA1_ENABLE_FOLLOW_ROMEN is ignored
                if((myPlugin->regA1 & REGA1_WAIT_FOR_RET) == 0)
                {
                    myPlugin->regA1 &= ~REGA1_ENABLE_FOLLOW_ROMEN;
                }                 
            }
            else
            {
                myPlugin->regA0 = myPlugin->commandValue;
            }
            break;
    }

    //kprintf("[ST] REG_A0 = &%02lx\n", myPlugin->regA0);
    //kprintf("[ST] REG_A1 = &%02lx\n", myPlugin->regA1);
    //kprintf("[ST] SlotZ0 = &%02lx\n", myPlugin->slotZ0);
    //kprintf("[ST] SlotZ1 = &%02lx\n", myPlugin->slotZ1);

    myPlugin->commandMode = CMD_MODE_NONE;
    myPlugin->pfxCount = 0;
}

static VOID doWritePhase(struct PluginData *myPlugin, LONG flashAddress, UBYTE flashData)
{
    // Flash is organized as 128 sector of 4K and a special sequence is
    // required to determine which sector should be erases then written

    //kprintf("[WR] Write detected phase: %2ld (flashData=&%02lx, flashAddress=0x%08lx)\n", myPlugin->writePhase, flashData, flashAddress);

    // Erase/Write unlock
         if(myPlugin->writePhase == 0 && flashData == 0xaa && flashAddress == 0x5555) { myPlugin->writePhase++; }
    else if(myPlugin->writePhase == 1 && flashData == 0x55 && flashAddress == 0x2aaa) { myPlugin->writePhase++; }
    // Erase
    else if(myPlugin->writePhase == 2 && flashData == 0x80 && flashAddress == 0x5555) { myPlugin->writePhase = 8; }
    else if(myPlugin->writePhase == 8 && flashData == 0xaa && flashAddress == 0x5555) { myPlugin->writePhase++; }
    else if(myPlugin->writePhase == 9 && flashData == 0x55 && flashAddress == 0x2aaa) { myPlugin->writePhase++; }
    // Actual erase
    else if(myPlugin->writePhase == 10 && flashData == 0x30)
    {
        memset(&myPlugin->rom[flashAddress & ~0xfff], 0x00, 0x1000);
        myPlugin->writePhase = 0;
        myPlugin->romModified = TRUE;
    }
    // Write
    else if(myPlugin->writePhase == 2 && flashData == 0xa0 && flashAddress == 0x5555) { myPlugin->writePhase = 16; }
    // Actual write
    else if(myPlugin->writePhase == 16)
    {
        myPlugin->rom[flashAddress] |= flashData;
        myPlugin->writePhase = 0;
        myPlugin->romModified = TRUE;
    }
    // In any other cas we cancel write sequence
    else
    {
        myPlugin->writePhase = 0;
    }
}

static BOOL loadROM(struct PluginData *myPlugin, CONST_STRPTR romFile)
{
    if(romFile)
    {
        BPTR file = Open(romFile, MODE_OLDFILE);

        if(file)
        {
            memset(myPlugin->rom, 0, sizeof(myPlugin->rom));
            Read(file, myPlugin->rom, sizeof(myPlugin->rom));
            Close(file);
            return TRUE;
        }
    }
    return FALSE;
}

static BOOL saveROM(struct PluginData *myPlugin)
{
    BPTR file = Open(myPlugin->romFile, MODE_NEWFILE);

    if(file)
    {
        Write(file, myPlugin->rom, sizeof(myPlugin->rom));
        Close(file);
        return TRUE;
    }
    return FALSE;
}

static BOOL checkROMModified(struct PluginData *myPlugin)
{
    if(myPlugin->romFile && myPlugin->romModified)
    {
        myPlugin->romModified = FALSE;
        return saveROM(myPlugin);
    }
    return TRUE;
}



/*
** Functions called from the GUI
*/

VOID Plugin_SetStartMode(struct ACEpansionPlugin *plugin, ULONG startMode)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    ObtainSemaphore(myPlugin->sema);
    myPlugin->startMode = startMode;
    myPlugin->resetTrigger = TRUE;
    ReleaseSemaphore(myPlugin->sema);
}

VOID Plugin_SetROMFile(struct ACEpansionPlugin *plugin, CONST_STRPTR romFile)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    ObtainSemaphore(myPlugin->sema);

    if(!checkROMModified(myPlugin))
    {
        GUI_NotifyROMSaveError(myPlugin->common.ap_PrefsWindow, myPlugin->romFile);
    }

    if(loadROM(myPlugin, romFile))
    {
        FreeVec(myPlugin->romFile);
        myPlugin->romFile = StrDup(romFile);
    }
    else
    {
        GUI_NotifyROMLoadError(myPlugin->common.ap_PrefsWindow, romFile);
        GUI_SetROMFile(myPlugin->common.ap_PrefsWindow, DEFAULT_ROMPATH);
    }
    myPlugin->resetTrigger = TRUE;

    ReleaseSemaphore(myPlugin->sema);
}

