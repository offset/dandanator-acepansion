/*
** Graphical User Interface using the Be API
** for Dandanator ACEpansion
*/

#include <Alert.h>
#include <Box.h>
#include <Button.h>
#include <GroupView.h>
#include <LayoutBuilder.h>
#include <OptionPopUp.h>
#include <sys/haiku/PopASL.h>

#include <acepansion/lib_header.h>

#include "acepansion.h"
#include "interface.h"
#define CATCOMP_NUMBERS
#include "generated/locale_strings.h" /* catalog string ids */

static STRPTR StartModes[] =
{
    (STRPTR)MSG_STARTMODE_GAME,
    (STRPTR)MSG_STARTMODE_TEST,
    (STRPTR)MSG_STARTMODE_BASIC,
    NULL
};

static BOOL StartModesTranslated = FALSE;

class SettingsBox: public BBox
{
    public:
        SettingsBox(struct ACEpansionPlugin* plugin);

        void AttachedToWindow() override;
        void MessageReceived(BMessage*) override;

        BOptionPopUp* CYA_StartMode;
        BButton* BTN_Eject;
		PopASL* STR_ROMFile;
    private:
        struct ACEpansionPlugin* myPlugin;
};

SettingsBox::SettingsBox(struct ACEpansionPlugin* plugin)
    : BBox("config")
    , myPlugin(plugin)
{
    BGroupView* inner = new BGroupView(B_VERTICAL);
    AddChild(inner);

    CYA_StartMode = new BOptionPopUp("start mode", GetString(MSG_STARTMODE), new BMessage('DMOD'));
    for (int i = 0; StartModes[i]; i++)
        CYA_StartMode->AddOptionAt(StartModes[i], i, i);

    BLayoutBuilder::Group<>(inner)
        .SetInsets(B_USE_DEFAULT_SPACING)
        .Add(CYA_StartMode)
        .AddGroup(B_HORIZONTAL)
            .Add(STR_ROMFile = new PopASL('DROM', GetString(MSG_ROMFILE), NULL,
                GetString(MSG_ASL_REQUEST), "*.rom", GetString(MSG_ROMFILE_HELP)))
            .Add(BTN_Eject = new BButton("eject", GetString(MSG_EJECT_BUTTON), new BMessage('DROM')))
        .End()
    .End();

    STR_ROMFile->SetMessage(new BMessage('DROM'));
}

void SettingsBox::AttachedToWindow()
{
    STR_ROMFile->SetTarget(this);
    CYA_StartMode->SetTarget(this);
    BTN_Eject->SetTarget(this);
}

void SettingsBox::MessageReceived(BMessage* message)
{
    switch (message->what)
    {
        case 'DROM':
			Plugin_SetROMFile(myPlugin, message->GetString("Name"));
            break;
        case 'DMOD':
            Plugin_SetStartMode(myPlugin, message->FindInt32("be:value"));
            break;
        default:
            BBox::MessageReceived(message);
            break;
    }
}

/*
** Function which is called to initialize commons just
** after the library was loaded into memory and prior
** to any other API call
**
** This is the good place to open our required libraries
** and create our MUI custom classes
*/
BOOL GUI_InitResources(VOID)
{
	return TRUE;
}

/*
** Function which is called to free commons just before
** the library is expurged from memory
**
** This is the good place to close our required libraries
** and destroy our MUI custom classes
*/
VOID GUI_FreeResources(VOID)
{
}

/*
** Function which is called from CreatePlugin() to build the GUI
*/
Object* GUI_Create(struct ACEpansionPlugin* myPlugin)
{
    if(!StartModesTranslated)
    {
        STRPTR *str = StartModes;

        while(*str)
        {
            *str = GetString((intptr_t)*str);
            str++;
        }
        StartModesTranslated = TRUE;
    }

    BBox* box = new SettingsBox(myPlugin);
    box->SetLabel(GetString(MSG_CONFIG));

    return (Object*)box;
}

/*
** Function which is called from DeletePlugin() to delete the GUI
*/
VOID GUI_Delete(Object *gui)
{
	BView* view = (BView*)gui;
	BWindow* window = view->Window();
	window->Lock();
	view->RemoveSelf();
	window->Unlock();
	delete view;
}


/*
** Functions called from the plugin
*/
VOID GUI_SetStartMode(Object* gui, ULONG startMode)
{
	SettingsBox* view = (SettingsBox*)gui;
	view->CYA_StartMode->SetValue(startMode);
}

VOID GUI_SetROMFile(Object* gui, CONST_STRPTR ROMFile)
{
	SettingsBox* view = (SettingsBox*)gui;
	view->STR_ROMFile->TextControl()->SetText(ROMFile);
}

VOID GUI_NotifyROMLoadError(UNUSED Object *gui, CONST_STRPTR fileName)
{
	BString message;
	message.SetToFormat(GetString(MSG_ROM_LOAD_FAILED_S), fileName);
	BAlert* alert = new BAlert(GetString(MSG_TITLE), message.String(), GetString(MSG_OK));
	alert->Go();
}

VOID GUI_NotifyROMSaveError(UNUSED Object *gui, CONST_STRPTR fileName)
{
	BString message;
	message.SetToFormat(GetString(MSG_ROM_SAVE_FAILED_S), fileName);
	BAlert* alert = new BAlert(GetString(MSG_TITLE), message.String(), GetString(MSG_OK));
	alert->Go();
}

