/*
** Graphical User Interface using MUI
** for Dandanator ACEpansion
*/

#include <clib/alib_protos.h>

#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/intuition.h>
#include <proto/muimaster.h>

#include <libraries/asl.h>
#include <acepansion/lib_header.h>

#include "acepansion.h"
#include "interface.h"
#define CATCOMP_NUMBERS
#include "generated/locale_strings.h" /* catalog string ids */



struct Library *IntuitionBase;
struct Library *UtilityBase;
struct Library *MUIMasterBase;



struct MUI_CustomClass *MCC_DandanatorClass = NULL;



static STRPTR StartModes[] =
{
    (STRPTR)MSG_STARTMODE_GAME,
    (STRPTR)MSG_STARTMODE_TEST,
    (STRPTR)MSG_STARTMODE_BASIC,
    NULL
};

static BOOL StartModesTranslated = FALSE;



/*
** MCC_DandanatorClass definitions
*/
#define DandanatorObject NewObject(MCC_DandanatorClass->mcc_Class, NULL

#define MUIA_Dandanator_Plugin (TAG_USER | 0x0000) // [I..] struct ACEpansionPlugin *, Reference to the plugin managing the GUI (mandatory)

#define MUIM_Dandanator_Notify (TAG_USER | 0x0100) // struct MUIP_Dandanator_Notify, notify the plugin or the GUI about changes
    #define MUIV_Dandanator_Notify_ToGUI    0 // Notify the GUI
    #define MUIV_Dandanator_Notify_ToPlugin 1 // Notify the plugin
        // For both directions
        #define MUIV_Dandanator_Notify_StartMode 10 // Notify about run start mode
        #define MUIV_Dandanator_Notify_ROMFile   11 // Notify about ROM file name

struct MUIP_Dandanator_Notify
{
    IPTR id;
    IPTR direction;
    IPTR type;
    union
    {
        ULONG        startMode;
        CONST_STRPTR romFile;
    };
};

struct DandanatorData
{
    struct ACEpansionPlugin *plugin;

    Object *CYA_StartMode;
    Object *STR_ROMFile;
};



/*
** MCC_DandanatorClass methods
*/
static IPTR mNotify(struct IClass *cl, Object *obj, struct MUIP_Dandanator_Notify *msg)
{
    struct DandanatorData *data = INST_DATA(cl,obj);

    switch(msg->direction)
    {
        case MUIV_Dandanator_Notify_ToGUI:
            switch(msg->type)
            {
                case MUIV_Dandanator_Notify_StartMode:
                    nnset(data->CYA_StartMode, MUIA_Cycle_Active, msg->startMode);
                    break;
                case MUIV_Dandanator_Notify_ROMFile:
                    nnset(data->STR_ROMFile, MUIA_String_Contents, msg->romFile);
                    break;
            }
            break;

        case MUIV_Dandanator_Notify_ToPlugin:
            switch(msg->type)
            {
                case MUIV_Dandanator_Notify_StartMode:
                    Plugin_SetStartMode(data->plugin, msg->startMode);
                    break;
                case MUIV_Dandanator_Notify_ROMFile:
                    Plugin_SetROMFile(data->plugin, msg->romFile);
                    break;
            }
            break;
    }

    return 0;
}

static IPTR mNew(struct IClass *cl, Object *obj, Msg msg)
{

    struct TagItem *tags,*tag;
    struct DandanatorData *data;

    struct ACEpansionPlugin *plugin = NULL;

    Object *CYA_StartMode;
    Object *STR_ROMFile;
    Object *BTN_Eject;

    // Parse initial taglist
    for(tags=((struct opSet *)msg)->ops_AttrList; (tag=NextTagItem(&tags)); )
    {
        switch(tag->ti_Tag)
        {
            case MUIA_Dandanator_Plugin:
                plugin = (struct ACEpansionPlugin *)tag->ti_Data;
                break;                              
        }
    }

    if(!plugin) return 0;

    obj = (Object *)DoSuperNew(cl,obj,
    GroupFrameT(GetString(MSG_CONFIG)),
    MUIA_Group_Columns, 2,
    Child, Label2(GetString(MSG_STARTMODE)),
    Child, CYA_StartMode = CycleObject,
        MUIA_ShortHelp, GetString(MSG_RESTART_HELP),
        MUIA_Cycle_Entries, StartModes,
        MUIA_CycleChain, TRUE,
        End,
    Child, Label2(GetString(MSG_ROMFILE)),
    Child, HGroup,
        Child, PopaslObject,
            MUIA_ShortHelp, GetString(MSG_ROMFILE_HELP),
            MUIA_Popstring_String, STR_ROMFile = StringObject,
                StringFrame,
                MUIA_String_MaxLen, 512,
                MUIA_String_SpellChecking, FALSE,
                MUIA_CycleChain, TRUE,
                End,
            MUIA_Popstring_Button, TextObject,
                ButtonFrame,
                MUIA_Weight, 0,
                MUIA_InputMode , MUIV_InputMode_RelVerify,
                MUIA_Background, MUII_ButtonBack,
                MUIA_CycleChain, TRUE,
                MUIA_Text_Contents, "\33I[6:19]", // MUII_PopFile
                End,
            ASLFR_TitleText, GetString(MSG_ASL_REQUEST),
            ASLFR_DoPatterns, TRUE,
            ASLFR_InitialPattern, "#?.rom",
            End,
        Child, BTN_Eject = TextObject,
            ButtonFrame,
            MUIA_Weight, 0,
            MUIA_InputMode , MUIV_InputMode_RelVerify,
            MUIA_Background, MUII_ButtonBack,
            MUIA_CycleChain, TRUE,
            MUIA_Text_Contents, GetString(MSG_EJECT_BUTTON),
            End,
        End,
        TAG_MORE,((struct opSet *)msg)->ops_AttrList);

    if(!obj) return 0;

    data = INST_DATA(cl,obj);

    data->plugin = plugin;

    data->STR_ROMFile = STR_ROMFile;
    data->CYA_StartMode = CYA_StartMode;

    DoMethod(STR_ROMFile, MUIM_Notify, MUIA_String_Acknowledge, MUIV_EveryTime,
             obj, 4, MUIM_Dandanator_Notify, MUIV_Dandanator_Notify_ToPlugin,
                     MUIV_Dandanator_Notify_ROMFile, MUIV_TriggerValue);
    DoMethod(BTN_Eject, MUIM_Notify, MUIA_Pressed, FALSE,
             obj, 4, MUIM_Dandanator_Notify, MUIV_Dandanator_Notify_ToPlugin,
                     MUIV_Dandanator_Notify_ROMFile, NULL);
    DoMethod(CYA_StartMode, MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime,
             obj, 4, MUIM_Dandanator_Notify, MUIV_Dandanator_Notify_ToPlugin,
                     MUIV_Dandanator_Notify_StartMode, MUIV_TriggerValue);

    return (IPTR)obj;
}



/*
** MUI Custom Class dispatcher, creation & destruction
*/
DISPATCHER(DandanatorClass)
{
    switch (msg->MethodID)
    {
        case OM_NEW                 : return(mNew   (cl,obj,(APTR)msg));
        case MUIM_Dandanator_Notify : return(mNotify(cl,obj,(APTR)msg));
    }
}
DISPATCHER_END



/*
** Function which is called to initialize commons just
** after the library was loaded into memory and prior
** to any other API call
**
** This is the good place to open our required libraries
** and create our MUI custom classes
*/
BOOL GUI_InitResources(VOID)
{
    UtilityBase = OpenLibrary("utility.library", 0L);
    IntuitionBase = OpenLibrary("intuition.library", 0L);
    MUIMasterBase = OpenLibrary("muimaster.library", 0L);

    if(UtilityBase && IntuitionBase && MUIMasterBase)
    {
        MCC_DandanatorClass = MUI_CreateCustomClass(
            NULL,
            MUIC_Group,
            NULL,
            sizeof(struct DandanatorData),
            DISPATCHER_REF(DandanatorClass));
    }

    return MCC_DandanatorClass != NULL;
}

/*
** Function which is called to free commons just before
** the library is expurged from memory
**
** This is the good place to close our required libraries
** and destroy our MUI custom classes
*/
VOID GUI_FreeResources(VOID)
{
    if(MCC_DandanatorClass)
        MUI_DeleteCustomClass(MCC_DandanatorClass);

    CloseLibrary(MUIMasterBase);
    CloseLibrary(IntuitionBase);
    CloseLibrary(UtilityBase);
}



/*
** Function which is called from CreatePlugin() to build the GUI
*/
Object * GUI_Create(struct ACEpansionPlugin *plugin)
{
    if(!StartModesTranslated)
    {
        STRPTR *str = StartModes;

        while(*str)
        {
            *str = GetString((LONG)*str);
            str++;
        }
        StartModesTranslated = TRUE;
    }

    return DandanatorObject,
        MUIA_Dandanator_Plugin, plugin,
        End;
}

/*
** Function which is called from DeletePlugin() to delete the GUI
*/
VOID GUI_Delete(Object *gui)
{
    MUI_DisposeObject(gui);
}



/*
** Functions called from the plugin
*/
VOID GUI_SetStartMode(Object *gui, ULONG startMode)
{
    DoMethod(gui, MUIM_Dandanator_Notify, MUIV_Dandanator_Notify_ToGUI,
             MUIV_Dandanator_Notify_StartMode, startMode);
}

VOID GUI_SetROMFile(Object *gui, CONST_STRPTR ROMFile)
{
    DoMethod(gui, MUIM_Dandanator_Notify, MUIV_Dandanator_Notify_ToGUI,
             MUIV_Dandanator_Notify_ROMFile, ROMFile);
}

VOID GUI_NotifyROMLoadError(UNUSED Object *gui, CONST_STRPTR fileName)
{
    MUI_Request(NULL, NULL, 0, GetString(MSG_TITLE), GetString(MSG_OK), GetString(MSG_ROM_LOAD_FAILED_S), fileName);
}

VOID GUI_NotifyROMSaveError(UNUSED Object *gui, CONST_STRPTR fileName)
{
    MUI_Request(NULL, NULL, 0, GetString(MSG_TITLE), GetString(MSG_OK), GetString(MSG_ROM_SAVE_FAILED_S), fileName);
}


