## Version $VER: dandanator.acepansion.catalog 1.0 (17.07.2022)
## Languages english fran�ais deutsch espa�ol
## Codeset english 0
## Codeset fran�ais 0
## Codeset deutsch 0
## Codeset espa�ol 0
## SimpleCatConfig CharsPerLine 200
## Header Locale_Strings
## TARGET C english "generated/locale_strings.h" NoCode
## TARGET CATALOG fran�ais "Release/Catalogs/fran�ais/" Optimize
## TARGET CATALOG deutsch "Release/Catalogs/deutsch/" Optimize
## TARGET CATALOG espa�ol "Release/Catalogs/espa�ol/" Optimize
MSG_MENU_TOGGLE
Plug a Dandanator
Brancher une Dandanator
Dandanator anschlie�en
Conecte un Dandanator
;
MSG_MENU_PREFS
Dandanator configuration...
Configuration de la Dandanator...
Dandanator-Konfiguration ...
Configuraci�n del Dandanator...
;
MSG_TITLE
Dandanator
Dandanator
Dandanator
Dandanator
;
MSG_CONFIG
Configuration
Configuration
Konfiguration
Configuraci�n
;
MSG_ROMFILE
ROM:
ROM :
ROM:
ROM:
;
MSG_ROMFILE_HELP
ROM file to load into the Dandanator.
Fichier ROM � charger dans la Dandanator.
ROM-Datei zum Laden in den Dandanator.
Cargar una ROM en el Dandanator.
;
MSG_ASL_REQUEST
Select the ROM file to load...
S�lectionnez le fichier ROM � charger...
W�hle die zu ladende ROM-Datei aus ...
Eliga la ROM a cargar...
;
MSG_ROMFILE_FAILED_S
\ecImpossible to load ROM file\n"%s".
\ecImpossible de lire fichier ROM\n� %s �.
\ecLaden der ROM-Datei\n�%s� unm�glich.
\ecImposible cargar la ROM\n"%s".
;
MSG_OK
  Ok\x20\x20
  Ok\x20\x20
  Ok\x20\x20
  Ok\x20\x20
;
MSG_STARTMODE
Start:
D�marrage :
Start:
Arranque:
;
MSG_STARTMODE_GAME
Game
Jeu
Spiel
Juego
;
MSG_STARTMODE_TEST
Test
Test
Test
Prueba
;
MSG_STARTMODE_BASIC
Basic
Basic
Basic
Basic
;
MSG_RESTART_HELP
Choose how the Dandanator should behave at CPC start-up. In "Game" mode, it will run the game loaded in its ROM. In "Test" mode, it will launch the test routine of the ROM (not supported by all ROMs). \
In "Basic" mode, it will let the CPC boot normally.
Choix de la fa�on dont la Dandanator doit se comporter au d�marrage du CPC. En mode "Jeu", elle va lancer le jeu charg� dans sa ROM. En mode "Test", elle va lancer la routine de test de la ROM (non \
support� par toutes les ROMs). En mode "Basic", elle va laisser le CPC d�marrer normallement.
W�hlen Sie, wie sich der Dandanator beim Starten des CPC verhalten soll. Im �Game�-Modus startet er das im ROM geladene Spiel. Im �Test�-Modus startet er die Testroutine des ROMs (wird nicht von allen ROMs unterst�tzt). \
Im �Basic�-Modus l�sst er den CPC normal hochfahren.
Eliga como debe comportarse el Dandanator al arrancar el CPC. En modo "Juego", correrr� el juego cargado en su ROM. En modo "Prueba",  lanzar� la rutina ha rutina de comprobaci�n de la ROM (no est� \
sportado en todas las ROMs). En modo "Basic", dejar� el arranque normal.
;
MSG_EJECT_BUTTON
  Eject\x20\x20
  �jecter\x20\x20
  Auswerfen\x20\x20
  Expulsar\x20\x20
;
MSG_ROM_LOAD_FAILED_S
\ecImpossible to load ROM\n"%s".
\ecImpossible de lire la ROM\n� %s �.
\ecUnm�glich ROM �%s� zu laden.
\ecImposible cargar el ROM\n� %s �.
;
MSG_ROM_SAVE_FAILED_S
\ecImpossible to save modification to ROM\n"%s".
\ecImpossible de sauver les modifications de la ROM\n� %s �.
\ecEs ist unm�glich, die �nderung auf dem ROM\n�%s� zu speichern.
\ecImposible guardar las modificaciones en el ROM\n� %s �.
;

