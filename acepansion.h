/*
** dandanator.acepansion public API
*/

#ifndef ACEPANSION_H
#define ACEPANSION_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#ifndef ACEPANSION_PLUGIN_H
#include <acepansion/plugin.h>
#endif


#define LIBNAME "dandanator.acepansion"
#define VERSION 1
#define REVISION 1
#define DATE "21.10.2023"
#define COPYRIGHT "� 2022-2023 Philippe Rimauro"

#define API_VERSION 7


/*
** Dandanator plugin specific API to be called from GUI
*/
VOID Plugin_SetStartMode(struct ACEpansionPlugin *plugin, ULONG startMode);
VOID Plugin_SetROMFile(struct ACEpansionPlugin *plugin, CONST_STRPTR romFile);


#endif /* ACEPANSION_H */

